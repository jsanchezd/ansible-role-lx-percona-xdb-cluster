---
- name: Install Pre-requisites
  import_tasks: prerequisites.yml

- name: Import Debian/Ubuntu tasks
  include_tasks: deb-repo.yml
  when: ansible_facts.os_family|lower == 'debian'

- name: Import RedHat/CEntOS tasks
  include_tasks: rh-repo.yml
  when: ansible_facts.os_family|lower in ['redhat','rocky']

- name: Enable the Repository
  command: percona-release enable-only pxc-{{ mysql_version|replace('.','') }} release

- name: Enable Tools Release
  command: percona-release enable tools release

- name: Install Debian/Ubuntu package
  import_tasks: deb-install.yml
  when:
    - ansible_facts.os_family|lower == 'debian'

- name: Install RedHat/CEntOS package
  import_tasks: rh-install.yml
  when:
    - ansible_facts.os_family|lower in ['redhat','rocky']

- name: Copy my.cnf to /etc/my.cnf/
  template:
    src: cluster-my.cnf.j2
    dest: /etc/my.cnf.d/cluster-my.cnf

- name: Fix /var/log/mysqld.log permissions
  file:
    path: /var/log/mysqld.log
    owner: mysql
    group: mysql
    mode: 0766

- name: Open Firewall ports of Debian/Ubuntu
  include_tasks: deb-firewall.yml
  when:
    - ansible_facts.os_family|lower == 'debian'

- name: Open Firewall ports of RedHat/CentOS/Rocky
  include_tasks: rh-firewall.yml
  when:
    - ansible_facts.os_family|lower in ['redhat','rocky']

- name: Enable and start the service
  service:
    name: mysql
    state: started
    enabled: no
  when: 
    - ansible_host == pxc["nodes"]["seed"]

- name: Include First time install tasks
  import_tasks: set_root_pswd.yml
  when:
    - deb_first_install.changed or rh_first_install.changed
    - ansible_host == pxc["nodes"]["seed"]

- name: Set MySQL root passwordless access
  template:
    src: root.my.cnf.j2
    dest: /root/.my.cnf

- name: Stop regular MySQL Service
  service:
    name: mysql
    state: stopped
    enabled: no
  when: 
    - ansible_host == pxc["nodes"]["seed"]

- name: Bootstrap first node of the cluster
  service:
    name: mysql@bootstrap
    state: started
    enabled: yes
  when: 
    - ansible_host == pxc["nodes"]["seed"]

- name: Start other nodes
  service:
    name: mysql
    state: started
    enabled: yes
  when: 
    - ansible_host in pxc["nodes"]["non_seed"]
  throttle: 1